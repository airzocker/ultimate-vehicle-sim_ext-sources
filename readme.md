# This repo is only for the external sources of the "ultimate-vehicle-sim_unity3d" (https://gitlab.com/airzocker/ultimate-vehicle-sim_unity3d.git) project.

* Just clone it anywhere, because you don't have to use it with Unity3d.

* Image sources, audio sources, model sources and similar can be committed to the corresponding folder in this repo, being one of the following source file types:
    * .xcf (gimp; images)
    * .mmpz (lmms; audio) with all the used audio files in the same directory
    * .blend (blender; models)
    * other free file formats for sources can be requested

* Then just pull up a merge request for the feature branch with the new (media) sources